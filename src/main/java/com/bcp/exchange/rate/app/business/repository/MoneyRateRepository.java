package com.bcp.exchange.rate.app.business.repository;

import java.util.Collection;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.bcp.exchange.rate.app.business.model.MoneyRate;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MoneyRateRepository extends ReactiveCrudRepository<MoneyRate, Long> {

	@Query("SELECT * FROM money_rate WHERE symbol in (:symbols)")
	Flux<MoneyRate> findBySymbolIn(Collection<String> symbols);

	@Query("SELECT TOP 1 * FROM money_rate WHERE symbol = :symbols")
	Mono<MoneyRate> findBySymbol(String symbols);
}
