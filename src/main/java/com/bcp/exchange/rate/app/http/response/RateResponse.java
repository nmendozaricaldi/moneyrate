package com.bcp.exchange.rate.app.http.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateResponse {
    private String from;
    private String to;
    private Double amount;
    private Double rate;
    private Double value;
}
