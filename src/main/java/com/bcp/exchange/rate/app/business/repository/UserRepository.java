package com.bcp.exchange.rate.app.business.repository;

import com.bcp.exchange.rate.app.business.model.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface UserRepository extends ReactiveCrudRepository<User, String> {
}
