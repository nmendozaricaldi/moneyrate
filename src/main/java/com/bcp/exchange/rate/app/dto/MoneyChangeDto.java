package com.bcp.exchange.rate.app.dto;

import java.util.Date;
import java.util.List;

import com.bcp.exchange.rate.app.business.model.MoneyRate;
import com.bcp.exchange.rate.app.serializer.MoneyChangeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(using = MoneyChangeSerializer.class)
public class MoneyChangeDto {
	private String base;
	private Date date;
	private List<MoneyRate> rates;
}
