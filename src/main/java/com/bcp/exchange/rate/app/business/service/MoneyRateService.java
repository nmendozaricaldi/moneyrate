package com.bcp.exchange.rate.app.business.service;

import java.util.Collection;

import com.bcp.exchange.rate.app.business.model.MoneyRate;
import com.bcp.exchange.rate.app.http.request.RateRequest;
import com.bcp.exchange.rate.app.http.response.RateResponse;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MoneyRateService {
	Flux<MoneyRate> findBySymbols(Collection<String> symbols);

	Mono<MoneyRate> findBySymbol(String symbol);

	Mono<MoneyRate> save(MoneyRate moneyRate);
	
	Flux<MoneyRate> findAll();
	
	Mono<Void> delete(MoneyRate moneyRate);

	Mono<MoneyRate> findById(Long id);

	Mono<RateResponse> calculate(RateRequest mr);
}
