package com.bcp.exchange.rate.app;

import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.bcp.exchange.rate.app.business.model.MoneyRate;

@AutoConfigureWebTestClient
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class SpringBootMoneyRateApplicationTests {
	
	@Autowired
	private WebTestClient client;
		
//	@Value("${config.base.endpoint}")
	private String url ="/api/rates";

	@Test
	public void listarTest() {
		
		client.get()
		.uri(url)
		.accept(MediaType.APPLICATION_JSON)
		.header("Authorization", "Bearer eyJhbGciOiJIUzUxMiJ9.eyJyb2xlIjpbIlJPTEVfQURNSU4iXSwic3ViIjoiYWRtaW4iLCJpYXQiOjE1ODE2MDM1NTYsImV4cCI6MTU4MTYzMjM1Nn0.0UumM7NmDqcHnR2hCG7dHXYr2V6x9sVqHU7cENgNPZVHXvCnOeJb88C0YaVXKDb7uz8pYrly1z-wiXjZGIMO_A")
		.exchange()
		.expectStatus().isOk()
		.expectHeader().contentType(MediaType.APPLICATION_JSON)
		.expectBodyList(MoneyRate.class)
		.consumeWith(response -> {
			List<MoneyRate> productos = response.getResponseBody();
			productos.forEach(p -> {
				System.out.println(p.getSymbol());
			});
			
			Assertions.assertThat(productos.size()>0).isTrue();
		});
		//.hasSize(9);
	}
	
}
