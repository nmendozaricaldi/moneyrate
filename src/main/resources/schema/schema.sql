CREATE TABLE categoria(
	id INTEGER IDENTITY PRIMARY KEY,
	nombre VARCHAR(255));

CREATE TABLE user(
username VARCHAR(255) PRIMARY KEY,
password VARCHAR(255),
enabled BOOLEAN,
roles VARCHAR(255)
);

CREATE TABLE producto(
	id INTEGER IDENTITY PRIMARY KEY,
	nombre VARCHAR(255),
	precio DOUBLE,
	create_at TIMESTAMP,
	foto VARCHAR(255));
	
--DROP TABLE money_rate;

CREATE TABLE money_rate
(
	id INTEGER IDENTITY PRIMARY KEY,
    symbol VARCHAR(255),
    rate DOUBLE,
    create_at TIMESTAMP
)
   
   
