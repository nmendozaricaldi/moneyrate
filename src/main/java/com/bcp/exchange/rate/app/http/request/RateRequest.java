package com.bcp.exchange.rate.app.http.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RateRequest {
	@NotNull
	private String from;
	@NotNull
	private String to;
	@NotNull
	@Min(0)
	private Double amount;
}
