package com.bcp.exchange.rate.app.business.service.impl;

import com.bcp.exchange.rate.app.business.repository.UserRepository;
import com.bcp.exchange.rate.app.business.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import com.bcp.exchange.rate.app.business.model.User;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Mono<User> findByUsername(String username) {
		return userRepository.findById(username);
	}
}
