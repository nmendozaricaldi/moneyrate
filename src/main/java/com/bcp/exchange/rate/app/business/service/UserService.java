package com.bcp.exchange.rate.app.business.service;

import com.bcp.exchange.rate.app.business.model.User;
import reactor.core.publisher.Mono;

public interface UserService {
    Mono<User> findByUsername(String username);
}
