package com.bcp.exchange.rate.app.business.controller;

import com.bcp.exchange.rate.app.business.model.MoneyRate;
import com.bcp.exchange.rate.app.business.service.MoneyRateService;
import com.bcp.exchange.rate.app.dto.MoneyChangeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/rates")
public class MoneyRateController {

	@Autowired
	private MoneyRateService rateService;

	@RequestMapping(method = RequestMethod.GET, value = "/latest")
	public Mono<MoneyChangeDto> process(
			@RequestParam(value = "symbols", required = false, defaultValue = "EU") Collection<String> symbols,
			@RequestParam(value = "BASE", required = false, defaultValue = "USD") String base) {

		Flux<MoneyRate> moneyRates = rateService.findBySymbols(symbols);
		return moneyRates.collectList().map(rates -> {
			MoneyChangeDto changeDto = new MoneyChangeDto();
			changeDto.setRates(rates);
			changeDto.setBase(base);
			changeDto.setDate(new Date());
			return changeDto;
		});
	}

	@GetMapping
	public Flux<MoneyRate> list() {
		return rateService.findAll();
	}

	@PostMapping
	@PreAuthorize("hasRole('ADMIN')")
	public Mono<ResponseEntity<Map<String, Object>>> create(@Valid @RequestBody Mono<MoneyRate> moneyRate) {

		Map<String, Object> response = new HashMap<String, Object>();

		return moneyRate.flatMap(mr -> {
			if (mr.getCreateAt() == null) {
				mr.setCreateAt(LocalDateTime.now());
			}

			return rateService.save(mr).map(p -> {
				response.put("producto", p);
				response.put("mensaje", "MoneyRate creado con Exito");
				response.put("timestamp", new Date());
				return ResponseEntity.created(URI.create("/api/rates/".concat(String.valueOf(p.getId()))))
						.contentType(MediaType.APPLICATION_JSON).body(response);
			});

		}).onErrorResume(t -> {
			return Mono.just(t).cast(WebExchangeBindException.class).flatMap(e -> Mono.just(e.getFieldErrors()))
					.flatMapMany(Flux::fromIterable)
					.map(fieldError -> "El campo " + fieldError.getField() + " " + fieldError.getDefaultMessage())
					.collectList().flatMap(list -> {
						response.put("errors", list);
						response.put("timestamp", new Date());
						response.put("status", HttpStatus.BAD_REQUEST.value());
						return Mono.just(ResponseEntity.badRequest().body(response));
					});

		});

	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<MoneyRate>> findById(@PathVariable Long id) {
		return rateService.findById(id)
				.map(p -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(p))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@PutMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Mono<ResponseEntity<MoneyRate>> edit(@RequestBody MoneyRate moneyRate, @PathVariable Long id) {
		return rateService.findById(id).flatMap(mr -> {
			mr.setRate(moneyRate.getRate());
			return rateService.save(mr);
		}).map(p -> ResponseEntity.created(URI.create("/api/rates/".concat(String.valueOf(p.getId()))))
				.contentType(MediaType.APPLICATION_JSON).body(p)).defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public Mono<ResponseEntity<Void>> delete(@PathVariable Long id) {
		return rateService.findById(id).flatMap(p -> {
			return rateService.delete(p).then(Mono.just(new ResponseEntity<Void>(HttpStatus.NO_CONTENT)));
		}).defaultIfEmpty(new ResponseEntity<Void>(HttpStatus.NOT_FOUND));
	}

}
