DELETE FROM money_rate;
--user:user
INSERT INTO user (username,password,enabled,roles)
VALUES ('user', 'cBrlgyL2GI2GINuLUUwgojITuIufFycpLG4490dhGtY=',true,'ROLE_USER');
--admin:admin
INSERT INTO user (username,password,enabled,roles)
VALUES ('admin', 'dQNjUIMorJb8Ubj2+wVGYp6eAeYkdekqAcnYp+aRq5w=',true,'ROLE_ADMIN');

INSERT INTO money_rate (id, symbol, rate) VALUES (1,'PEN', 1);
INSERT INTO money_rate (id, symbol, rate) VALUES (2,'EU', 1.079);
INSERT INTO money_rate (id, symbol, rate) VALUES (3,'USD', 3.339);