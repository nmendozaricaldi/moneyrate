package com.bcp.exchange.rate.app.business.controller;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.bcp.exchange.rate.app.http.request.RateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.support.WebExchangeBindException;

import com.bcp.exchange.rate.app.business.model.MoneyRate;
import com.bcp.exchange.rate.app.business.service.MoneyRateService;
import com.bcp.exchange.rate.app.dto.MoneyChangeDto;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/change")
public class MoneyChangeController {

	@Autowired
	private MoneyRateService rateService;

	@PostMapping
	public Mono<ResponseEntity<Map<String, Object>>> calculate(@Valid @RequestBody Mono<RateRequest> rateRequest) {

		Map<String, Object> response = new HashMap<String, Object>();

		return rateRequest.flatMap(mr -> {
			return rateService.calculate(mr).map(p -> {
				response.put("change", p);
				response.put("mensaje", "MoneyRate creado con Exito");
				response.put("timestamp", new Date());
				return ResponseEntity.ok()
						.contentType(MediaType.APPLICATION_JSON).body(response);
			});

		}).onErrorResume(t -> {
			return Mono.just(t).cast(WebExchangeBindException.class).flatMap(e -> Mono.just(e.getFieldErrors()))
					.flatMapMany(Flux::fromIterable)
					.map(fieldError -> "El campo " + fieldError.getField() + " " + fieldError.getDefaultMessage())
					.collectList().flatMap(list -> {
						response.put("errors", list);
						response.put("timestamp", new Date());
						response.put("status", HttpStatus.BAD_REQUEST.value());
						return Mono.just(ResponseEntity.badRequest().body(response));
					});

		});

	}
}
