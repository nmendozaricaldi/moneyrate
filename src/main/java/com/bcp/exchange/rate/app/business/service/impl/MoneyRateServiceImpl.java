package com.bcp.exchange.rate.app.business.service.impl;

import java.util.Collection;

import com.bcp.exchange.rate.app.http.request.RateRequest;
import com.bcp.exchange.rate.app.http.response.RateResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bcp.exchange.rate.app.business.model.MoneyRate;
import com.bcp.exchange.rate.app.business.repository.MoneyRateRepository;
import com.bcp.exchange.rate.app.business.service.MoneyRateService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MoneyRateServiceImpl implements MoneyRateService {

    private static final String baseCoin = "PEN";

    @Autowired
    private MoneyRateRepository repository;

    @Override
    public Flux<MoneyRate> findBySymbols(Collection<String> symbols) {
        return repository.findBySymbolIn(symbols);
    }

    @Override
    public Mono<MoneyRate> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Mono<RateResponse> calculate(RateRequest mr) {
        return repository.findBySymbol(mr.getFrom()).flatMap(from -> {
            return repository.findBySymbol(mr.getTo()).map(to -> {
                        if (baseCoin.equalsIgnoreCase(from.getSymbol())) {
                            return buildResponse(mr, to);
                        } else {
                            return buildResponse(mr, from, to);
                        }
                    }
            ).defaultIfEmpty(new RateResponse());
        }).defaultIfEmpty(new RateResponse());
    }

    private RateResponse buildResponse(RateRequest rateRequest, MoneyRate rate) {
        RateResponse response = new RateResponse();
        BeanUtils.copyProperties(rateRequest, response);

        response.setValue(rateRequest.getAmount() / rate.getRate());
        response.setRate(1 / rate.getRate());
        return response;
    }

    private RateResponse buildResponse(RateRequest rateRequest, MoneyRate from, MoneyRate to) {
        RateResponse response = new RateResponse();
        BeanUtils.copyProperties(rateRequest, response);

        response.setValue((rateRequest.getAmount() * from.getRate())/ to.getRate());
        response.setRate(to.getRate() / from.getRate());
        return response;
    }

    @Override
    public Mono<MoneyRate> findBySymbol(String symbol) {
        return repository.findBySymbol(symbol);
    }

    @Override
    public Mono<MoneyRate> save(MoneyRate moneyRate) {
        return repository.save(moneyRate);
    }

    @Override
    public Flux<MoneyRate> findAll() {
        return repository.findAll();
    }

    @Override
    public Mono<Void> delete(MoneyRate moneyRate) {
        return repository.delete(moneyRate);
    }

}
